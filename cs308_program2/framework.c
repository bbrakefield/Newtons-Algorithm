#include <windows.h>
#include <stdio.h>

void sqrtX(double n, double * result);
static char buf[255];
static char inputLabel[255];

/* disables warning for strcpy use */
#pragma warning(disable : 4996)

/* prototype for user's main program */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
    double x = 9.0;
    double number = 0;
	sqrtX(x, &number);
    printf("%f", number);
}


