; Assembler program to find the square root of a floating point number using Newton's algorithm.
; Author:  Brannon Brakefield
; Date:    11/16

.586
.MODEL FLAT
.STACK 4096
.DATA
accuracy    REAL8   0.00001
lower       REAL8   1.0
upper       REAL8   ?
guess       REAL8   ?
number      REAL8   ?
two         REAL8   2.0

.CODE
compare MACRO N

        fcomp N
        fstsw ax
        sahf
ENDM

_sqrtX  PROC

        push ebp
        mov ebp,esp
        push ebx
 
        finit                       

        ; LOAD FIRST ARG BY VALUE ONTO STACK

        fld REAL8 PTR [ebp+8]       ; acts as upper value on first interation
        fst number                  ; save value in number variable
        fstp upper                   ; also save it in upper variable

        ; DO SQRT CALC HERE

    whileLoop:
        fld upper
        fld lower
        fsub                        ; upper - lower stored in s(1)
                                
        compare accuracy            ; compare s(0) to accuracy and save status word
        jbe exitLoop                ; if less that or equal to ACCURACY exit the loop to return root

        fld upper                   ; reload upper value
        fld lower
        fadd                        ; pop st(0) and st(1), add values
                                    ; and push the sum onto the stack

        fdiv two                   ;  guess = (upper + lower) / 2
        fst guess               
        fmul guess                  ;  guess * guess

        compare number              ; (guess * guess) > number
        jbe lessThanEqualToNumber
        fld guess                   ; reload guess to top of stack to store value in variable
        fstp upper                   ; upper = guess
        jmp whileLoop               ; loop back

        lessThanEqualToNumber:
        fld guess                   ; reload guess to top of stack to store value in variable
        fstp lower                   ; lower = guess
        jmp whileLoop

    exitLoop:
     
        ; STORE SECOND ARG BY POINTER FROM THE STACK

        fld lower
        fld upper
        fadd
        fdiv two

        mov ebx, [ebp+16]           ; move address of second argument into register
        fstp REAL8 PTR [ebx]        ; move square root into register
        pop ebx
        pop ebp
        mov eax,0                   ; exit with return code 0
        ret
_sqrtX  ENDP
END                                 ; end of source code

